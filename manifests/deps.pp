# == Class: etcddiscovery::deps
#
#  etcddiscovery anchors and dependency management
#
class etcddiscovery::deps {
  anchor { 'etcddiscovery::install::begin': }
  -> Package<| tag == 'etcddiscovery'|>
  ~> anchor { 'etcddiscovery::install::end': }
  -> anchor { 'etcddiscovery::config::begin': }
  -> Etcddiscovery_config<||>
  ~> anchor { 'etcddiscovery::config::end': }
  ~> anchor { 'etcddiscovery::service::begin': }
  ~> Service<| tag == 'etcddiscovery-service' |>
  ~> anchor { 'etcddiscovery::service::end': }

  # Installation changes will always restart services.
  Anchor['etcddiscovery::install::end'] ~> Anchor['etcddiscovery::service::begin']
}
