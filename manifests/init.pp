# == Class: etcddiscovery
#
#  Cinder base package & configuration
#
# === Parameters
#
# [*package_ensure*]
#    (Optional) Ensure state for package.
#    Defaults to 'present'
#
# [*prefix_url*]
#    (Optional) Prefix URL.
#    Defaults to 'http://example.com'
#
# [*web_service_address*]
#    (Optional) Address where to bind.
#    :8087 works, as well as IP:8087
#    Defaults to ':8087'
#
# [*endpoint_location*]
#    (Optional) Where is located your ETCD cluster?
#     Default to: 'http://127.0.0.1:2379'
#
# [*enabled*]
#   (optional) The state of the service (boolean value)
#   Defaults to true
#
# [*manage_service*]
#   (optional) Whether to start/stop the service (boolean value)
#   Defaults to true
#
class etcddiscovery (
  $package_ensure      = 'present',
  $package_name        = 'etcd-discovery',
  $prefix_url          = 'http://example.com',
  $web_service_address = ':8087',
  $endpoint_location   = 'http://127.0.0.1:2379',
  $enabled             = true,
) inherits etcddiscovery::params{

  include etcddiscovery::deps

  package { 'etcddiscovery':
    ensure => $package_ensure,
    name   => $package_name,
    tag    => ['openstack', 'etcddiscovery'],
  }

  etcddiscovery_config {
    'server/prefix_url':          value => $prefix_url;
    'server/web_service_address': value => $web_service_address;
    'etcd/endpoint_location':     value => $endpoint_location;
  }

  if $enabled {
    if $manage_service {
      $ensure = 'running'
    }
  } else {
    if $manage_service {
      $ensure = 'stopped'
    }
  }

  service { 'etcd-discovery':
    ensure    => $ensure,
    name      => $::etcddiscovery::params::etcddiscovery_service,
    enable    => $enabled,
    hasstatus => true,
    tag       => 'etcddiscovery-service',
  }
}
