# == Class: etcddiscovery::params
#
# Parameters for puppet-etcddiscovery
#
class etcddiscovery::params {
  if $::osfamily == 'Debian' {
    $package_name          = 'etcd-discovery'
    $etcddiscovery_service = 'etcd-discovery'
  } elsif($::osfamily == 'RedHat') {
    $package_name          = undef
    $etcddiscovery_service = undef
  }
}
